#!/usr/bin/env python

#===============================================================================
# SMAPapp_ChromPlots.py
#===============================================================================

#Yves BAWIN February 2023
#Python script to create chromosome plots based on a SMAP haplotypes table.
#
#===============================================================================
# Import modules
#===============================================================================

import os, argparse, natsort, warnings
from datetime import datetime
import pandas as pd
warnings.simplefilter(action='ignore', category=pd.errors.PerformanceWarning)
import multiprocessing as mp
import matplotlib as mpl
import matplotlib.pyplot as plt
mpl.use('Agg')

#===============================================================================
# Parse arguments
#===============================================================================

#Create an ArgumentParser object
parser = argparse.ArgumentParser(description = 'Create chromosome plots in circular format or as horizontal bar charts showing the similarity of samples with predefined reference samples .')

'''Mandatory arguments.'''
parser.add_argument('-t', '--table',
                    type = str,
                    help = 'Name of the haplotypes table retrieved from SMAP haplotype-sites or SMAP haplotype-windows in the input directory.')
parser.add_argument('-b', '--bed',
                    type = str,
                    help = 'BED file containing the coordinates of each contig in the reference genome sequence. The BED file must be stored in the input directory.')

'''Input data options.'''
parser.add_argument('-i', '--input_directory',
                    default = '.',
                    type = str,
                    help = 'Input directory containing the haplotypes table, the --samples text file, the --loci text file, --reference_samples text file, and the --reference_samples_table file (default = current directory).')
parser.add_argument('-n', '--samples',
                    type = str,
                    default = None,
                    help = 'Name of a tab-delimited text file in the input directory defining the order of the (new) sample names in the barplot: first column = old names, second column (optional) = new names (default = no sample list, the order of samples in the bar plot equals their order in the haplotypes table).')
parser.add_argument('-l', '--loci',
                    type = str,
                    default = None,
                    help = 'Name of a tab-delimited text file in the input directory containing a one-column list of locus IDs formatted as in the haplotypes table (default = no list provided).')
parser.add_argument('-r', '--reference_samples',
                    type = str,
                    default = None,
                    help = 'Name of a tab-delimited text file in the input directory listing the (new) IDs of samples used as references in the plot: first column = sample name, second column (optional): colour ID (default = no list with reference samples IDs is not provided).')
parser.add_argument('-a', '--additional_table',
                    type = str,
                    default = None,
                    help = 'Name of an additional haplotypes table retrieved from SMAP haplotype-sites or SMAP haplotype-windows in the input directory. (default = no additional haplotypes table provided).')
parser.add_argument('--markers',
                    type = str,
                    default = None,
                    help = 'BED file containing the coordinates of each HiPlex marker (default = no BED file provided).')
parser.add_argument('-c', '--centromeres',
                    type = str,
                    default = None,
                    help = 'Name of the tab-delimited text file with centromere positions (default = no centromeres file provided).')
parser.add_argument('-sp', '--species',
                    type = str,
                    default = 'Species Unknown',
                    help = 'Species name of the reference genome sequence (default = Species Unknown).')

'''Analysis options.'''
parser.add_argument('--ploidy',
                    type = int,
                    default = 2,
                    help = 'Integer defining the (highest) ploidy level of the samples in the haplotypes table (default = 2, diploid).')
parser.add_argument('--ignore_unassigned',
                    dest = 'no_unassigned',
                    action = 'store_true',
                    help = 'Do not include loci with unassigned haplotypes in the output (default = data on unassigned haplotypes are included).')
parser.add_argument('-u', '--chromosome_unit',
                    type = int,
                    default = 1000000,
                    help = 'Chromosome unit in base pairs (default = 1 Mbp).')
parser.add_argument('-m', '--merge_distance',
                    type = int,
                    default = 0,
                    help = 'Maximum genomic distance (in base pairs) between two haplotypes to be assigned to the same region (default = 0 bp).')
parser.add_argument('-p', '--processes',
                    default = 4,
                    type = int,
                    help = 'Number of processes used by the script (default = 4).')

'''Output data options.'''
parser.add_argument('-v, --verbose',
                    dest = 'verbose',
                    action = 'store_true',
                    help = 'Print information about ignored loci to the terminal (default = no information printed).')
parser.add_argument('-o', '--output_directory',
                    default = '.',
                    type = str,
                    help = 'Output directory (default = current directory).')
parser.add_argument('-s', '--suffix',
                    default = None,
                    type = str,
                    help = 'Suffix added to all output file names (default = no suffix added).')
parser.add_argument('--circos_directory',
                    default = '.',
                    type = str,
                    help = 'Directory of the Circos input files that are created by the script (default = current directory).')
parser.add_argument('--bar_width',
                    default = 0.05,
                    type = float,
                    help = 'Width of the chromosome bars in the graphs (default = 0.5).')
parser.add_argument('--bar_distance',
                    default = 0.1,
                    type = float,
                    help = 'Float number defining the width between sample bars in the graphs (default = 0.1).')
parser.add_argument('--plot_reference',
                    dest = 'plot_refs',
                    action = 'store_true',
                    help = 'Include reference samples in the plots (default = reference samples are not included).')
parser.add_argument('--plot_format',
                    choices = ['pdf', 'png', 'svg', 'jpg', 'jpeg', 'tif', 'tiff'],
                    default = "pdf",
                    help = 'File format of plots (default = pdf).')
parser.add_argument('-f', '--font',
                    choices = ['Times New Roman', 'Arial', 'Calibri', 'Consolas', 'Verdana', 'Helvetica', 'Comic Sans MS'],
                    default = 'Times New Roman',
                    help = 'Font used in all plots (default = Times_New_Roman, other options are: Arial, Calibri, Consolas, Verdana, Helvetica, and Comic_Sans_MS).')
parser.add_argument('--title_fontsize',
                    type = int,
                    default = 12,
                    help = 'Title font size in points (default = 12).')
parser.add_argument('--label_fontsize',
                    type = int,
                    default = 12,
                    help = 'Label font size in points (default = 12).')
parser.add_argument('--tick_fontsize',
                    type = int,
                    default = 8,
                    help = 'Tick font size in points,(default = 8).')
parser.add_argument('--plot_resolution',
                    type = int,
                    default = 300,
                    help = 'Plot resolution in dots per inch (dpi) (default = 300).')
parser.add_argument('--colours',
                    type = str,
                    default = 'red, blue, green, purple, orange, yellow',
                    help = 'Comma-separated list of colours used in the plot (default = red, blue, green, purple, orange, yellow).')
parser.add_argument('--print_haplotypes_table',
                    dest = 'print_haplotypes',
                    action = 'store_true',
                    help = 'Print a table with the haplotypes after the removal of non-polymorphic loci and loci with missing data in the reference samples (default = haplotypes table is not printed).')
parser.add_argument('--print_sample_information',
                    dest = 'print_sample_info',
                    action = 'store_true',
                    help = 'Print a table with the haplotype count or the similarity to the reference samples (columns) for each sample (rows) to a tab-delimited text file (default = sample information is not printed).')
parser.add_argument('--print_locus_information',
                    dest = 'print_locus_info',
                    action = 'store_true',
                    help = 'Print a table with the haplotype count or the similarity to the reference samples (columns) for each locus in each sample (rows) to a tab-delimited text file (default = locus information is not printed).')
parser.add_argument('--value_type',
                    choices = ['count', 'percentage'],
                    default = 'count',
                    help = 'Type of values in the --print_sample_information output table. Two options are allowed: count and percentage (default = count).')
parser.add_argument('--plot_type',
                    choices = ['Circos', 'Linear', 'All'],
                    default = 'Circos',
                    help = 'Type of the plots (default = Circos, the other option are: Linear and All (Circos and Linear plots)).')
parser.add_argument('--marker_type',
                    choices = ['point', 'pixel', 'triangle_down', 'triangle_up', 'triangle_left', 'triangle_right', 'tri_down', 'tri_up', 'tri_left', 'tri_right', 'octagon', 'square', 'pentagon', 'star', 'hexagon1', 'hexagon2', 'plus', 'x', 'diamond', 'thin_diamond', 'vline', 'hline', 'plus_filled', 'x_filled', 'tickleft', 'tickright', 'tickup', 'tickdown', 'caretleft', 'caretright', 'caretup', 'caretdown', 'caretleftbase', 'caretrightbase', 'caretupbase', 'caretdownbase'],
                    default = 'triangle_right',
                    help = 'Marker type used in linear plots (default = triangle_right, the other option are listed on the Matplotlib website: https://matplotlib.org/stable/api/markers_api.html).')
parser.add_argument('--plot_regions',
                    type = str,
                    default = None,
                    help = 'BED file containing the coordinates of the regions displayed in linear plots (default = no BED file provided).')

#Parse arguments to a dictionary
args = vars(parser.parse_args())

#===============================================================================
# Functions
#===============================================================================
def print_date():
    print('-------------------')
    print(datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
    print('-------------------\n')
    return


def import_table(table, i = args['input_directory']):
    return pd.read_csv('{}/{}'.format(args['input_directory'].rstrip('/'), table), sep = '\t', dtype = str, index_col = ['Reference', 'Locus', 'Haplotypes'])


def txt2dict(original, file = args['samples'], opt = 'Sample', i = args['input_directory'], n = args['samples']):
    new = dict()
    lists = {'Sample' : '--samples', 'Locus' : '--loci', 'Ref_sample' : '--reference_samples'}
    for index, line in enumerate(open('{}/{}'.format(i.rstrip('/'), file))):
        line = line.rstrip().rstrip('\t')
        if line:
            line = line.split('\t')
            assert line[0] in original, '{} {} on line {} in the {} list is not listed in the {}. Please check if the IDs in the {} list exactly match with those in the {}.'.format(opt if opt != 'Ref_sample' else 'Sample', line[0], index + 1, lists[opt], 'haplotypes table' if not n or opt == 'Sample' else '--samples list', lists[opt], 'haplotypes table' if not n or opt == 'Sample' else '--samples list')
            if opt != 'Locus':
                assert line[0] not in new, 'Sample {} on line {} in the {} list is listed twice. Please make sure that each sample is only mentioned once in this list.'.format(line[0], index + 1, lists[opt])
                if opt == 'Ref_sample' and len(line) > 1:
                    assert line[1] not in new.values(), 'Sample {} on line {} in the --reference_samples list has the same colour as sample {}. Please specify a unique colour for each reference sample.'.format(line[0], index + 1, [k for k, v in new if v == line[1]][0])
            if len(line) == 1:
                line.append(None)
            new[line[0]] = line[1]
    return new


def bed2dict(f, marker = False, i = args['input_directory']):
    bed_dict = dict()
    for l in open('{}/{}'.format(i, f)):
        l = l.rstrip().rstrip('\t')
        if l:
            l = l.split('\t')
            if l[1] == l[2]:
                l[2] = int(l[2]) + 1
            if not marker:
                assert l[0] not in bed_dict, '{} is mentioned multiple times in {}. Please make sure that each element in the first column of this file is unique.'.format(l[0], f)
                bed_dict[l[0]] = (int(l[1]), int(l[2]))
            else:
                if len(l) == 3:
                    l.append('green')
                if l[0] not in bed_dict:
                    bed_dict[l[0]] = [(int(l[1]), int(l[2]), l[3])]
                else:
                    bed_dict[l[0]].append((int(l[1]), int(l[2]), l[3]))
    return bed_dict


def rename_samples(t):
    IDs = txt2dict(samples)
    for sample in samples:
        #Remove samples from the haplotypes table that are not in the sampleIDs list.
        if sample not in IDs:
            t.drop(sample, axis = 1, inplace = True)
        elif IDs[sample]:
            pool = [ID for ID in IDs if IDs[ID] == IDs[sample]]
            if len(pool) > 1 and IDs[sample] not in t:
                #Pool samples with the same sample ID.
                print('\n\t- The samples {} and {} were combined into one sample with ID {}.\n'.format(', '.join(pool[:-1]), pool[-1], IDs[sample]))
                t[IDs[sample]] = t.loc[:,pool].astype(float).max(axis = 1, numeric_only = True)
                t.drop(pool, axis = 1, inplace = True)
            else:
                #Rename samples if a new ID is specified.
                t.rename(columns={sample:IDs[sample]}, inplace = True)
                
    #Remove haplotypes with only zero calls after the removal of samples.
    t.drop(t[t.astype(float).sum(axis = 1) == 0].index, inplace = True)
    
    #Reorder the columns in the dataframe based on the order of the samples in IDs.
    order = list()
    for old, new in IDs.items():
        if new:
            if new not in order:
                order.append(new)
        else:
            order.append(old)
    t = t[order]
    return t


def get_colour_IDs(ref_samples, colours = args['colours'], no_unassigned = args['no_unassigned']):
    #Define a list of colours.
    colour_list = colours.split(',')
    
    #Iterate over the reference samples.
    for index, sample in enumerate(ref_samples.keys()):
        if ref_samples[sample]:
            #Check if the colour name is in the colour list.
            if ref_samples[sample].lower() in mpl.colors.CSS4_COLORS:
                colour = ref_samples[sample].lower()
            
            #Get the name of colours specified in Hexadecimal code (HEX).
            elif _type == 'Linear' and ref_samples[sample].upper() in mpl.colors.CSS4_COLORS.values() or ref_samples[sample].upper() in colours:
                colour = ref_samples[sample].upper()
                
            #Get the name from a colour specified in Red - Green - Blue code (RGB).
            else:
                colour = tuple([int(x.strip('()')) for x in ref_samples[sample].split(',')])
                assert len(colour) == 3, 'The colour {} of sample {} in the --reference_sample list is not valid. Please provide a CSS colour name as listed on https://matplotlib.org/stable/gallery/color/named_colors.html, a hexadecimal (HEX) colour code starting with a #, or a rgb colour code consisting of 3 numbers ranging between 0 and 255 that are separated by a comma (e.g. 0, 0, 0).'.format(ref_samples[sample], sample)
        else:
            colour = colour_list[index % len(colour_list)]
            
        #Add the colour code to the ref_samples dictionary.
        ref_samples[sample] = colour
    
    #Add an unassigned and unknown category to the reference samples. If already present, the names of these categories are capitalized.
    if not no_unassigned:
        if 'unassigned' not in ref_samples and 'Unassigned' not in ref_samples:
            ref_samples['Unassigned'] = 'grey'
        elif 'unassigned' in ref_samples:
            ref_samples['Unassigned'] = ref_samples.pop('unassigned')
    if 'unknown' not in ref_samples and 'Unknown' not in ref_samples:
        ref_samples['Unknown'] = 'black'
    elif 'unknown' in ref_samples:
        ref_samples['Unknown'] = ref_samples.pop('unknown')
    return ref_samples


def check_samples(sample, ploidy = args['ploidy'], no_unassigned = args['no_unassigned'], m = args['merge_distance'], plot_ref = args['plot_refs']):
    #Create the list 'counts' to store the count data.
    count_df = pd.DataFrame(index = pd.MultiIndex.from_arrays([[],[],[]], names = ('Sample', 'Reference', 'Locus')), columns = ref_IDs)
    
    #Iterate over all chromosome references in the haplotypes table.
    for ref in ref_chroms:
        #Extract the locus names on the current reference and define the variables to keep track of the previous locus in the reference.
        loci = t.loc[ref].index.get_level_values('Locus').unique()
        prev_start, prev_end, prev_unassigned = int(), int(), list()
        
        #Iterate over all loci in the haplotypes table.
        for locus in loci:
            start, end = int(locus.split(':')[1].split('-')[0]), int(locus.split(':')[1].split('-')[1].split('_')[0])
            calls = t.loc[(ref, locus), sample].fillna(0).astype(float)
            
            #Only consider loci in the sample without missing data.
            if sum(calls) > 0:
                #Extract the reference calls for the considered locus.
                ref_calls = t.loc[(ref, locus), [x for x in ref_IDs if x not in ('Unassigned', 'Unknown')]].astype(float)
                if not no_unassigned:
                    ref_calls['Unassigned'] = 0
                ref_calls['Unknown'] = 0
                ref_calls = ref_calls[ref_samples.keys()]
                
                #if sample == 'Coffea_canephora_YB550' and '{}:{}-{}'.format(ref, start, end) == 'chr0:8645192-8645253':
                    #print(sample)
                #Iterate over the calls in the locus and keep track of the number of haplotypes in the sample.
                NumberOfHaplotypes = 0
                count = [0] * len(ref_calls.columns)
                for index, call in calls.items():
                    #Ignore the haplotype if it was not called in the sample (call = 0).
                    if call > 0:
                        hcount = ref_calls.loc[index].tolist()
                        NumberOfHaplotypes += 1
                        #Check the number of times the haplotype is present in the reference samples. Store the count data in the counts list.
                        hcount = check_unassigned_unknown(call, hcount)
                        count = [x + y for x, y in zip(count, hcount)]
                        
                #Adjust the haplotype calls to the ploidy level if only one match was found.
                if len([x for x in count if x > 0]) == 1:
                    count = [x if x == 0 else ploidy for x in count]
                elif len([x for x in count if x > 0]) < ploidy:
                    count[ref_IDs.index('Unassigned')] += ploidy - len([x for x in count if x > 0])
                    
                #Ignore loci with more haplotypes than the ploidy level.
                if 0 < NumberOfHaplotypes <= ploidy:
                    export_loci, export_unassigned = False, False
                    #Check if the locus is not the first locus of the reference. If it is the first locus, store the locus in the count dict and move forward to the next locus.
                    if prev_end:
                        #Check if the distance between the end coordinate of the previous locus and the start coordinate of the current locus is smaller than the predefined threshold. If not, write the previous loci to the output table and set the current locus as the first locus in a new round of comparisons.
                        if start - prev_end <= m:
                            if not no_unassigned:
                                count_wo_unassigned = [x for i, x in enumerate(count) if i != ref_IDs.index('Unassigned')]
                                #Check if both loci have identical counts. If not, write all previous loci to the output table and set the current locus as the first locus in a new round of comparisons.
                                if count != prev_count:
                                    #If the loci are different, check if the current locus only has unassigned calls. If not, write all previous loci to the output table and set the current locus as the first locus in a new round of comparisons.
                                    if sum(count_wo_unassigned) == 0:
                                        if prev_unassigned:
                                            prev_unassigned = [prev_unassigned[0], end, count]
                                        else:
                                            prev_unassigned = [start, end, count]
                                    else:
                                        if prev_unassigned:
                                            export_unassigned = True
                                        export_loci = True
                                else:
                                    prev_end = end
                                    prev_unassigned = list()
                            else:
                                export_loci = True
                        else:
                            if prev_unassigned:
                                export_unassigned = True
                            export_loci = True
                    else:
                        prev_start = start
                        prev_end = end
                        prev_count = count
                        
                    if export_loci:
                        locus_ID = '{}:{}-{}'.format(ref, prev_start, prev_end)
                        count_df.loc[(sample, ref, locus_ID), ] = prev_count
                        prev_start = start
                        prev_count = count
                        prev_end = end
                        
                    if export_unassigned:
                        locus_ID = '{}:{}-{}'.format(ref, prev_unassigned[0], prev_unassigned[1])
                        count_df.loc[(sample, ref, locus_ID), ] = prev_unassigned[2]
                        prev_unassigned = list()
                    
                    #Write the last locus of a chromosome to the counts table.
                    if prev_end and locus == t.loc[ref].index.get_level_values('Locus').to_list()[-1]:
                        if prev_unassigned:
                            locus_ID = '{}:{}-{}'.format(ref, prev_unassigned[0], prev_unassigned[1])
                            count_df.loc[(sample, ref, locus_ID), ] = prev_unassigned[2]
                        locus_ID = '{}:{}-{}'.format(ref, prev_start, prev_end)
                        count_df.loc[(sample, ref, locus_ID), ] = prev_count
                        
    #Remove reference samples from count table if preferred.
    if not plot_ref:
        count_df.drop([x for x in samples if x in ref_IDs], level = 0, inplace = True)
    return count_df


def check_unassigned_unknown(call, count, no_unassigned = args['no_unassigned']):
    count_values = [x for x in count if x > 0]
    
    #Add the haplotype count/frequency to the unknown category if the haplotype is not present in any reference sample.
    if len(count_values) == 0:
        count_index = ref_IDs.index('Unknown')
    
    #Add the haplotype count/frequency to one of the reference sample with the unique haplotype.
    elif len(count_values) == 1:
        count_index = count.index(count_values[0])
    
    #Add the haplotype count/frequency to the unassigned category if the haplotype is present in more than one reference sample.
    elif not no_unassigned:
        count_index = ref_IDs.index('Unassigned')
    else:
        count_index = None
        
    count = [0] * len(count)
    if count_index is not None:
        count[count_index] = call
    return count

def create_karyotype(o = args['output_directory'], s = args['suffix']):
    out = open('{}/Karyotype_{}.txt'.format(o, s), 'w+')
    for i, chrom in enumerate(natsort.natsorted(list(genome_dict.keys()))):
        print('chr - {} chr{} {} {} {}'.format(id_dict[chrom], i + 1, genome_dict[chrom][0], genome_dict[chrom][1], 'white'), file = out)
    print(file = out)
    if cent_dict:
        for chrom in natsort.natsorted(cent_dict):
            print('band {} p36.33 p36.33 {} {} {}'.format(id_dict[chrom], cent_dict[chrom][0], cent_dict[chrom][1], 'black'), file = out)
        print(file = out)
    if markers_dict:
        for chrom in natsort.natsorted(markers_dict):
            for band in natsort.natsorted(markers_dict[chrom]):
                print('band {} p36.33 p36.33 {} {} {}'.format(id_dict[chrom], band[0], band[1], band[2]), file = out)
    return


def create_config(o = args['output_directory'], s = args['suffix']):
    create_main(o, s)
    create_ideogram(o, s)
    create_ticks(o, s)
    create_housekeeping(o, s)
    return


def create_main(o, s, ploidy = args['ploidy'], u = args['chromosome_unit'], cdir = args['circos_directory']):
    out = open('{}/Circos_{}.conf'.format(o, s), 'w+')
    print('#Circos.conf\n', file = out)
    #Define karyotype file.
    print('Karyotype = {}/Karyotype_{}.txt\n'.format(cdir, s), file = out)
    print('Chromosomes_units = {}\n'.format(u), file = out)
    #Define position and size of ideogram.
    print('<<include {}/Ideogram_{}.conf>>'.format(cdir, s), file = out)
    #Define ticks and tick labels.
    print('<<include {}/Ticks_{}.conf>>'.format(cdir, s), file = out)
    #Define general parameters like colors, fonts, patterns and system settings.
    print('<<include {}/Housekeeping.conf>>'.format(cdir), file = out)
    #Define the colour panel of fonts.
    print('<<include etc/colors.conf>>', file = out)
    print('<<include etc/colors_fonts_patterns.conf>>\n', file = out)
    #Define image parameters (size, location, ...) in image block.
    print('<image>\n<<include etc/image.conf>>\n</image>\n', file = out)
    #Define tiles.
    NumberOfLayers = len(t.keys()) * ploidy
    r1 = 1.03
    print('<plots>', end = '', file = out)
    for sample in samples:
        r1 = round(r1 - 0.03, 2)
        for i in range(ploidy):
            r0 = round(r1 - 0.02, 2)
            print('\n<plot>\ntype    = tile\nshow    = yes\nr0     = {}r\nr1 =     {}r'.format(r0, r1), file = out)
            print('thickness = 15\npadding = 8\norientation = out\nbackground       = no\nmargin = 0u\nstroke_thickness = 0', file = out)
            print('file = {}/{}_{}{}.txt\n</plot>'.format(cdir, sample, s, i + 1), file = out)
            r1 = r0
    print('</plots>', file = out)
    return


def create_ideogram(o, s):
    out = open('{}/Ideogram_{}.conf'.format(o, s), 'w+')
    print('#Ideogram.conf\n', file = out)
    print('<ideogram>\n', file = out)
    print('<spacing>', file = out)
    print('default = 0.005r', file = out)
    print('</spacing>\n', file = out)
    print('radius           = 0.90r\nthickness        = 50p\nstroke_color     = black\nstroke_thickness = 2', file = out)
    print('fill             = yes\nfill_color       = black\nshow_bands            = yes', file = out)
    print('show_label     = yes\nlabel_font     = condensedbold\nlabel_radius   = dims(ideogram,radius) + 0.05r\nlabel_size     = {}\nlabel_parallel = yes'.format(font_set['label']['fontsize']), file = out)
    print('band_stroke_thickness = 0\nfill_bands            = yes\nband_transparency     = 1', file = out)
    print('</ideogram>', file = out)
    return


def create_ticks(o, s):
    out = open('{}/Ticks_{}.conf'.format(o, s), 'w+')
    print('#Ticks.conf\n', file = out)
    print('show_ticks          = yes\nshow_tick_labels    = yes\nchrticklabels       = yes\nchrticklabelfont    = {}'.format(font_set['ticks']['fontname']), file = out)
    print('grid_start         = dims(ideogram,radius_inner)-0.5r\ngrid_end           = dims(ideogram,radius_outer)+100\n', file = out)
    print('<ticks>', file = out)
    print('skip_first_label     = no\nskip_last_label      = no\nradius               = dims(ideogram,radius_outer)\ntick_separation      = 2p', file = out)
    print('min_label_distance_to_edge = 0p\nlabel_separation = 5p\nlabel_offset     = 2p\nlabel_size = 20p\nmultiplier = 1e-6\ncolor = black\n', file = out)
    print('<tick>', file = out)
    print('size           = 5p\nspacing        = 1u\nthickness      = 2p\ncolor          = black', file = out)
    print('show_label     = no\ngrid           = yes\ngrid_color     = grey\ngrid_thickness = 1p', file = out)
    print('</tick>\n', file = out)
    print('<tick>', file = out)
    print('size           = {}p\nspacing        = 10u\nthickness      = 2p\ncolor          = black\nshow_label     = yes\nlabel_size     = 15p\nlabel_offset   = 0p'.format(font_set['ticks']['fontsize']), file = out)
    print('format         = %.0f\ngrid           = yes\ngrid_color     = dgrey\ngrid_thickness = 1p', file = out)
    print('</tick>\n</ticks>', file = out)
    return


def create_housekeeping(o, s):
    out = open('{}/Housekeeping.conf'.format(o), 'w+')
    print('#Housekeeping.conf\n', file = out)
    print('anglestep       = 0.5\nminslicestep    = 10\nbeziersamples   = 40\ndebug           = no\nwarnings        = no\nimagemap        = no', file = out)
    print('paranoid        = no\nunits_ok        = bupr\nunits_nounit    = n\nfile_delim = \s\nfile_delim_collapse = yes', file = out)
    print('list_record_delim = \s*[;,]\s*\nlist_field_delim  = \s*[:=]\s*\noptions_record_delim = [,;]\noptions_field_delim  = =', file = out)
    print('skip_missing_expression_vars = no\nlegacy_underline_expression_syntax = no\nsvg_font_scale = 1.3\nsup_baseline_shift = 40', file = out)
    print('sub_baseline_shift = -40\nsup_fontsize = 90\nsub_fontsize = 90\ndefault_font   = default\ndefault_font_name  = Arial\ndefault_font_color = black', file = out)
    print('default_color  = black\n\n<guides>\nthickness      = 1\nsize           = 5\ntype           = outline\n\n<object>\nall            = no', file = out)
    print('ideogram       = no\nideogram_label = no\n</object>\n\n<color>\ndefault = lblue\ntext    = red\n</color>\n\n</guides>\n\ndebug_group = summary,output', file = out)
    print('debug_auto_timer_report = 30\ndebug_word_separator = " \ndebug_undef_text     = _undef_\ndebug_empty_text     = _emptylist_\ndebug_validate       = yes', file = out)
    print('debug_output_tidy    = no\ntext_pixel_subsampling = 1\ntext_snuggle_method    = array\nrestrict_parameter_names = no\ncase_sensitive_parameter_names = no', file = out)
    print('calculate_track_statistics = yes\ncolor_cache_static = yes\ncolor_cache_file   = circos.colorlist\ncolor_lists_use    = yes\nmemoize = yes', file = out)
    print('quit_on_dump = yes\noffsets = 0,0\nmax_ticks            = 5000\nmax_ideograms        = 200\nmax_links            = 1000000\nmax_points_per_track = 1000000', file = out)
    print('undefined_ideogram = skip\nrelative_scale_iterations = 10\nrelative_scale_spacing    = mode\ndata_out_of_range = trim,warn', file = out)
    print('track_defaults = etc/tracks\nround_brush_use           = yes\nround_brush_min_thickness = 5\nanti_aliasing = yes\nhousekeeping = yes\nauto_eval = no', file = out)
    return


def create_tiles(sample, o = args['output_directory'], s = args['suffix'], ploidy = args['ploidy'], m = args['merge_distance']):
    #Create a dictionary for every homolog.
    out = list()
    for i in range(ploidy):
        results_dict = dict()
        out.append(results_dict)
    
    #Iterate over the results and store them into a dictionary.
    for ref, results in count_df.loc[sample].iterrows():
        start, end = ref[1].split(':')[1].split('-')
        indexes = list(range(ploidy))
        while len(indexes) > 0:
            values = [(r, results[r]) for r in results.index if results[r] > 0]
            j = ref_IDs.index(values[0][0]) % ploidy
            while len(indexes) > 0 and j not in indexes:
                j = (j + 1) % ploidy
            indexes.remove(j)
            if ref[0] not in out[j]:
                out[j][ref[0]] = list()
            out[j][ref[0]].append([start, end, ref_samples[values[0][0]]])
            results.loc[values[0][0]] -= 1
            
    #Iterate over the loci in the dictionaries and merge them if their distance is smaller than the merge distance.
    for i, d in enumerate(out):
        for chrom, calls in d.items():
            n, j = 0, 1
            while n < len(calls) - j:
                if int(calls[n + j][0]) - int(calls[n][1]) < m:
                    if calls[n + j][2] == calls[n][2]:
                        calls[n] = [calls[n][0], calls[n + j][1], calls[n][2]]
                        for _ in range(j):
                            calls.remove(calls[n + 1])
                        j = 1
                    elif calls[n + j][2] == ref_samples['Unassigned']:
                        j += 1
                    else:
                        n += 1
                        j = 1
                else:
                    n += 1
                    j = 1
            out[i][chrom] = calls
            
    return out

def create_tile_output(o = args['output_directory'], s = args['suffix']):
    for sample, calls in regions_dict.items():
        for i, call in enumerate(calls):
            file = open('{}/{}_{}{}.txt'.format(o, sample, s, i + 1), 'w+')
            for chrom, loci in call.items():
                for locus in loci:
                    print('{} {} {} color={}'.format(id_dict[chrom], locus[0], locus[1], locus[2]), file = file)
    return

def set_font_style(f = args['font'], label_fsize = args['label_fontsize'], title_fsize = args['title_fontsize'], tick_fsize = args['tick_fontsize']):
    '''
    Set font settings for the plot title, axes titles, and the axes ticks.
    '''
    font_set = dict()
    font_set['title'] = {'fontname' : f, 'fontsize' : title_fsize}
    font_set['label'] = {'fontname' : f, 'fontsize' : label_fsize}
    font_set['ticks'] = {'fontname' : f, 'fontsize' : tick_fsize}
    return font_set


def plot_region(plot_dict, ploidy = args['ploidy'], out_dir = args['output_directory'], m_type = args['marker_type'], b_distance = args['bar_distance'], plot_format = args['plot_format'], bar_width = args['bar_width'], res = args['plot_resolution']):
    #Define all possible marker symbols.
    marker_symbols = {'point':'.', 'pixel':',', 'circle':'o', 'triangle_down':'v', 'triangle_up':'^', 'triangle_left':'<',
                      'triangle_right':'>', 'tri_down':'1', 'tri_up':'2', 'tri_left':'3', 'tri_right':'4', 'octagon':'8',
                      'square':'s', 'pentagon':'p', 'star':'*', 'hexagon1':'h', 'hexagon2':'H', 'plus':'+', 'x':'x', 'diamond':'D',
                      'thin_diamond':'d', 'vline':'|', 'hline':'_', 'plus_filled':'P', 'x_filled':'X', 'tickleft':'0', 'tickright':'1',
                      'tickup':'2', 'tickdown':'3', 'caretleft':'4', 'caretright':'5', 'caretup':'6', 'caretdown':'7', 'caretleftbase':'8',
                      'caretrightbase':'9', 'caretupbase':'10', 'caretdownbase':'11'}
                      
    #Iterate over the regions.
    for region in plot_dict.keys():
        #Define the figure settings and the list of samples.
        fig = plt.figure(figsize=(30, 20))
        ax = fig.add_subplot(111)
        chrom, pos = region.split(':')[0], region.split(':')[1].split('-')
        ybase = -0.2
        chrom_centers = list()
        plot_samples = list(plot_dict[region].keys())
        
        #Iterate over the samples.
        for sample in plot_samples[::-1]:
            ybase += b_distance
            if sample == 'Markers':
                chrom_center = ybase + bar_width/2
                bar = plt.scatter(tuple([x[0] for x in plot_dict[region][sample]['bars']]), (chrom_center, ) * len(plot_dict[region][sample]['bars']), c = tuple([x[0] for x in plot_dict[region][sample]['colours']]), marker = marker_symbols[m_type])
                ax.add_collection(bar)
                ybase += b_distance
            else:
                chrom_center = ybase + (bar_width * ploidy + 0.05 * (ploidy - 1))/2
                for p in reversed(range(ploidy)):
                    bar = mpl.collections.BrokenBarHCollection(plot_dict[region][sample][p]['bars'], (ybase, bar_width), facecolors = plot_dict[region][sample][p]['colours'], edgecolors = 'face')
                    ax.add_collection(bar)
                    ybase += b_distance
            chrom_centers.append(chrom_center)
        
        ax.set_yticks(chrom_centers)
        ax.set_yticklabels(plot_samples[::-1], fontdict = font_set['ticks'])
        ax.tick_params(axis='both', which='major', labelsize= font_set['label']['fontsize'])
        ax.axis('tight')
        plt.title(region, fontdict = font_set['title'])
        plt.xlabel('Genome coordinates', fontdict = font_set['label'])
        fig.savefig('{}/{}_{}_{}.{}'.format(out_dir, chrom, pos[0], pos[1], plot_format), format = plot_format, dpi = res)
        plt.close()
    return

#===============================================================================
# Script
#===============================================================================
if __name__ == '__main__':
    print_date()
    
    print(' * Loading input files ...')
    #Import master table with haplotype calls and extract sample names.
    t = import_table(args['table'])
    
    if args['additional_table']:
        #Import the additional haplotypes table.
        t2 = import_table(args['additional_table'])
        
        #Delete columns in the additional haplotypes table that are also present in the first table.
        deleted = t2[t2.columns.intersection(t.columns)].columns.tolist()
        t2.drop(deleted, axis = 1, inplace = True)
        if len(deleted) > 0:
            print('\n\t- {} samples were removed from the additional haplotypes table because they were present in the first haplotypes table:\n\t\t- {}'.format(len(deleted), '\n\t\t- '.join(deleted)))
        
        #Merge the two tables.
        t = t.merge(t2, left_index = True, right_index = True, how = 'outer')
        
    #Natural sort the loci in the haplotypes table.
    t.sort_values(by = "Locus", key = natsort.natsort_keygen(), inplace = True)
    
    #Get (and rename if specified) sample IDs from master table.
    samples = t.columns.tolist()
    if args['samples']:
        #Pool and/or rename samples if specified in the --samples list. Update the samples list.
        t = rename_samples(t)
        samples = t.columns.tolist()
        
    #Create a selection of loci (if specified). Remove loci from the haplotypes table if they are not present in the locus selection.
    if args['loci']:
        loci = t.index.get_level_values('Locus').unique().tolist()
        locusIDs = txt2dict(loci, args['loci'], 'Locus')
        deleted = [x for x in loci if x not in locusIDs]
        if len(deleted) > 0:
            print('\t- {} loc{} removed from the haplotypes table because {} not present in the --loci list:\n\t\t - {}\n'.format(len(deleted), 'i were' if len(deleted) > 1 else 'us was', 'it was' if len(deleted) == 1 else 'they were', '\n\t\t - '.join(deleted)))
            t.drop(deleted, level = 0, inplace = True)
        assert len(loci) > 0, 'No loci were retained after the removal of loci. Please disable the --loci option or list minimum one locus ID in the --loci list.'
        
        #Remove samples with only zero calls after the removal of loci.
        t.drop(t[t.astype(float).sum(axis = 0) == 0].index, inplace = True)
        
    #Remove loci with the same calls in every sample.
    invar_loci = t.nunique(axis = 1).groupby('Locus').sum().sort_index(key = natsort.natsort_keygen()) == t.groupby('Locus').size().sort_index(key = natsort.natsort_keygen())
    invar_loci = invar_loci[invar_loci].index
    t.drop(invar_loci, level = 'Locus', inplace = True)
    if args['verbose']:
        print('\n \t- {} loc{} ignored because they were not polymorphic within the sample set:\n\t\t- {}\n'.format(len(invar_loci), 'i were' if len(invar_loci) > 1 else 'us was', '\n\t\t- '.join(invar_loci)))
    
    #Import the list of reference samples. If no list with reference samples is provided, take the first sample in the haplotypes table as a reference.
    if args['reference_samples']:
        ref_samples = txt2dict(samples, args['reference_samples'], 'Ref_sample')
    else:
        ref_samples = {samples[0] : None}
        
    #Get the colour ID of the reference samples.
    ref_samples = get_colour_IDs(ref_samples)
    ref_IDs = [x for x in ref_samples.keys()]
    
    #Remove loci with missing data in at least one haplotype in the reference samples.
    na_in_ref = t[[x for x in ref_IDs if x not in ('Unassigned', 'Unknown')]].notna().all(axis = 1).groupby('Locus').all()
    na_in_ref = na_in_ref[na_in_ref == False].index
    t.drop(na_in_ref, level = 'Locus', inplace = True)
    if args['verbose']:
        print('\n \t- {} loc{} ignored because they contained missing data in one or more reference samples:\n\t\t- {}\n'.format(len(na_in_ref), 'i were' if len(na_in_ref) > 1 else 'us was', '\n\t\t- '.join(invar_loci)))
    
    #Remove haplotypes with zero calls in every sample.
    t.drop(t[t.astype(float).sum(axis = 1) == 0].index, inplace = True)
    
    #Convert genome coordinates into a dictionary.
    genome_dict = bed2dict(args['bed'])
    id_dict = {chrom:'{}{}'.format(''.join(e[0].lower() for e in args['species'].split(' ')), i + 1) for i, chrom in enumerate(natsort.natsorted(list(genome_dict.keys())))}
    
    #If specified, convert the centromere coordinates and marker coordinates into a dictionary.
    if args['centromeres']:
        cent_dict = bed2dict(args['centromeres'])
    else:
        cent_dict = None
        
    if args['markers']:
        markers_dict = bed2dict(args['markers'], marker = True)
    else:
        markers_dict = None
        
    #Create the output directory if the directory does not exist.
    if not os.path.isdir(args['output_directory']):
        os.mkdir(args['output_directory'])
        
    #Print the input haplotypes table after all filtering steps.
    if args['print_haplotypes']:
        t.to_csv('{}/Haplotypes_input{}.txt'.format(args['output_directory'], '_{}'.format(args['suffix']) if args['suffix'] else ''), sep = '\t')
        
    #Calculate the haplotype similarity between loci.
    print(' * Calculating the haplotype similarity between each sample and the reference sample{} ... '.format('s' if len(ref_samples) - 2 > 1 else ''))
    ref_chroms = t.index.get_level_values('Reference').unique()
    with mp.Pool(args['processes']) as p:
        count_dfs = p.map(check_samples, samples)
        
    #Convert the counts data into a dataframe with the samples as indexes and the counts as columns (type == count) or the samples and loci as indexes and the ref_samples as columns.
    count_df = pd.concat(count_dfs)
    samples = count_df.index.unique('Sample').to_list()
    
    #Define plot font settings.
    font_set = set_font_style()
    
    #Print locus information to a tab-delimited text file.
    if args['print_locus_info']:
        count_df.to_csv('{}/Loci{}.txt'.format(args['output_directory'], '_{}'.format(args['suffix']) if args['suffix'] else ''), sep = '\t')
    
    if args['print_sample_info']:
        if args['value_type'] == 'percentage':
            count_df.groupby('Sample', group_keys = True).apply(lambda x: 100 * x / x.sum().sum()).groupby(level = 0).sum().round(2).reindex(samples).to_csv('{}/Samples{}.txt'.format(args['output_directory'], '_{}'.format(args['suffix']) if args['suffix'] else ''), sep = '\t')
        else:
            count_df.groupby('Sample', group_keys = True).sum()
            
    #Create tile dictionaries.
    with mp.Pool(args['processes']) as p:
        regions_dicts = p.map(create_tiles, samples)
    regions_dict = dict(zip(samples, regions_dicts))
    
    #Create the chromosome plot.
    if args['plot_type'] in ['All', 'Circos']:
        print(' * Creating Circos input files ...')
        #Create karyotypes file.
        create_karyotype()
        
        #Create config input files.
        create_config()
        
        #Create tiles.
        create_tile_output()
        
    #Plot similarities.
    if args['plot_type'] in ['All', 'Linear']:
        print(' * Creating linear plots ...')
        if args['plot_regions']:
            plot_regions = bed2dict(args['plot_regions'], marker = True)
        else:
            plot_regions = {chrom:[genome_dict[chrom]] for chrom in genome_dict}
            
        plot_dict = dict()
        
        #Check if loci in the count dataframe overlap with the loci in the plot regions dictionary.
        for chrom, regions in plot_regions.items():
            for region in regions:
                region_ID = '{}:{}-{}'.format(chrom, region[0], region[1])
                plot_dict[region_ID] = dict()
                #Add the marker coordinates to the dictionary.
                if markers_dict and chrom in markers_dict:
                    plot_dict[region_ID]['Markers'] = dict()
                    plot_dict[region_ID]['Markers']['bars'] = [(x[0], int(x[1]) - int(x[0])) for x in markers_dict[chrom] if region[0] <= int(x[0]) <= region[1]]
                    plot_dict[region_ID]['Markers']['colours'] = [x[2] for x in markers_dict[chrom]]
                    
                #Iterate over all samples.
                for sample in samples:
                    for i, _ in enumerate(regions_dict[sample]):
                        if chrom in regions_dict[sample][i]:
                            for r in regions_dict[sample][i][chrom]:
                                start = None
                                #Bar in region.
                                if region[0] <= int(r[0]) and region[1] >= int(r[1]):
                                    start, stop = int(r[0]), int(r[1])
                                #Left part bar in region.
                                elif region[0] <= int(r[0]) <= region[1]:
                                    start, stop = int(r[0]), region[1]
                                #Right part bar in region.
                                elif region[0] <= int(r[1]) <= region[1]:
                                    start, stop = region[0], int(r[1])
                                if start:
                                    if sample not in plot_dict[region_ID]:
                                        plot_dict[region_ID][sample] = dict()
                                        for j in range(len(regions_dict[sample])):
                                            plot_dict[region_ID][sample][j] = dict()
                                            plot_dict[region_ID][sample][j]['bars'] = list()
                                            plot_dict[region_ID][sample][j]['colours'] = list()
                                            
                                    plot_dict[region_ID][sample][i]['bars'].append((start, stop - start))
                                    plot_dict[region_ID][sample][i]['colours'].append(r[2])
                #Ignore plots of regions without data.
                if len(plot_dict[region_ID]) == 0:
                    del plot_dict[region_ID]
                    
        plot_region(plot_dict)
    print(' * Finished!\n')
    print_date()