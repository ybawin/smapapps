#!usr/bin/python3

#===============================================================================
#AmpliconTraceOffTargets.py
#===============================================================================

# Yves BAWIN August 2019

#===============================================================================
# Import modules
#===============================================================================

import os, argparse
import pysam # make sure this is version 1.11
version = pysam.__version__
if not int(version.split('.')[1]) > 11 or (int(version.split('.')[1]) == 11 and int(version.split('.')[2]) >= 1):
    sys.exit('Please make sure Pysam version 0.11.1 (or later) is installed (current version = {})\nRun sudo pip3 install pysam==0.11.1 to fix this\n'.format(version))
import pandas as pd
import multiprocessing as mp
from datetime import datetime
from natsort import natsorted
from Bio import pairwise2

#===============================================================================
# Parse arguments
#===============================================================================

#Create an ArgumentParser object.
parser = argparse.ArgumentParser(description = 'Determine the read depth of target loci in a BED file and/or identify off-target loci.')

#Mandatory argument.
parser.add_argument('--bed',
                    type = str,
                    help = 'BED file containing amplicon regions (first column = contig/scaffold/chromosome, second column = start forward primer, third column = start reverse primer).')

#Input data options.
parser.add_argument('-i', '--input_directory',
                    default = '.',
                    type = str,
                    help = 'Input directory containing the BED file and optionally the primers file and samples list (default = current directory).')
parser.add_argument('-q', '--mapping_quality',
                    default = 20,
                    type = int,
                    help = 'Minimum mapping quality of reads (default = 20).')
parser.add_argument('-d', '--bam_directory',
                    default = '.',
                    type = str,
                    help = 'Directory containing BAM files (default = current directory).')
parser.add_argument('--primers',
                    default = None,
                    type = str,
                    help = 'Text file containing the primer sequences (default = no file provided).')

#Analysis options.
parser.add_argument('-r', '--min_nr_reads',
                    default = 10,
                    type = int,
                    help = 'Minimum number of reads per amplicon per genotype (default = 10).')
parser.add_argument('-n', '--samples',
                    default = None,
                    type = str,
                    help = 'Name and directory of a tab-delimited text file defining the order of the (new) sample IDs in the output file: first column = old IDs, second column (optional) = new IDs (default = no list provided).')
parser.add_argument('--trace_off-targets',
                    dest = 'off_targets',
                    action = 'store_true',
                    help = 'Trace for off-target loci in the BAM files (default = off-target reads are ignored).')
parser.add_argument('--min_length',
                    type = int,
                    default = 1,
                    help = 'Minimum length of identified off-target loci. Shorter loci will be ignored (default = 1 base pair).')
parser.add_argument('-p', '--processes',
                    default = 4,
                    type = int,
                    help = 'Number of processes used by the script (default = 4).')

'''
Output data options.
'''
parser.add_argument('-o', '--output_directory',
                    default = '.',
                    type = str,
                    help = 'Output directory (default = current directory).')
parser.add_argument('-s', '--suffix',
                    default = None,
                    type = str,
                    help = 'Suffix added to all output file names (default = no suffix added).')
parser.add_argument('-b', '--border_length',
                    type = int,
                    default = 10,
                    help = 'Border length for SMAP haplotype-window (default = 10).')

#Parse arguments to a dictionary.
args = vars(parser.parse_args())

#===============================================================================
# Functions
#===============================================================================
def print_date():
    '''
    Print the current date and time to stderr.
    '''
    print('-------------------')
    print(datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
    print('-------------------\n')
    return

def bed2dict(bed = args['bed'], i = args['input_directory']):
    '''
    Convert the BED file into a dictionary with the contig/scaffold/chromosome ID as keys and the start and end position of the amplicons as a list of tuples.
    '''
    bed_dict = dict()
    for line in open('{}/{}'.format(i, bed)):
        line = line.rstrip().split('\t')
        chrom, start, end = line[0], int(line[1]), int(line[2]) - 1
        if chrom not in bed_dict:
            bed_dict[chrom] = [[start, end]]
        else:
            bed_dict[chrom].append([start, end])
    return bed_dict

def primers2dict(primers = args['primers'], i = args['input_directory']):
    '''
    Convert the primers file into a dictionary with the primer ID as keys and a list of the primer sequences (normal and reverse complement) as values.
    '''
    normal, complement = 'ACTG', 'TGAC'
    primer_dict = dict()
    for line in open('{}/{}'.format(i, primers)):
        if not line.startswith('Seq ID'):
            line = line.rstrip().split('\t')
            if line[1] == 'Forward':
                ID, seq = line[0], line[4].lstrip('C')
            else:
                ID, seq = line[0], line[4].lstrip('G')
            rc = ''.join(e for e in [complement[normal.index(n)] for n in list(seq[::-1])])
            primer_dict[ID] = [seq, rc]
    return primer_dict

def similarity(seqA, seqB):
    return pairwise2.align.globalxx(seqA, seqB, score_only = True) / len(seqA)

def identify_off_targets(bam, dir = args['bam_directory'], min_MAPQ = args['mapping_quality'], min_length = args['min_length']):
    '''
    Identify non-target loci in a BAM file.
    '''
    #Extract the sample name.
    name = os.path.splitext(bam)[0]
    
    #Create two dictionaries to store off-target loci.
    off_target_locus_dict = dict()
    off_target_primer_dict = dict()
    
    #Open the BAM file.
    samfile = pysam.AlignmentFile(dir + '/' + bam, 'rb')
    
    #For each bam file, check all unique reads if they overlap with an amplicon or if the start and stop with a primer sequence.
    read_iter = samfile.fetch()
    for read in read_iter:
        #Ignore reads with a mapping quality score lower than the user-defined threshold.
        if int(read.mapping_quality) >= min_MAPQ:
            #Retrieve the contig ID, start coordinate, and end coordinate of the read.
            chrom = read.reference_name
            read_start = read.reference_start - read.query_alignment_start
            read_end = read_start + read.query_alignment_end
            
            #Create a variable to check if the locus to which the read aligns is identified.
            identified = False
            
            #Check if the read aligns to one of the target regions.
            if chrom in target_dict:
                i = 0
                while i < len(target_dict[chrom]) and not identified:
                    target = target_dict[chrom][i]
                    if (read_start <= target[0] and read_end >= target[1]) or read_start <= target[0] <= read_end or read_start <= target[1] <= read_end:
                        identified = True
                    i += 1
                    
            #Check if the read aligns to an identified off-target regions.
            if not identified and chrom in off_target_locus_dict:
                i = 0
                off_targets = list(off_target_locus_dict[chrom].keys())
                while i < len(off_target_locus_dict[chrom]) and not identified:
                    off_target = off_targets[i]
                    if (read_start <= off_target[0] and read_end >= off_target[1]) or read_start <= off_target[0] <= read_end or read_start <= off_target[1] <= read_end:
                        Fprimer = primer_dict[off_target_primer_dict['{}:{}-{}'.format(chrom, off_target[0] + 1, off_target[1] + 1)][0]]
                        Rprimer = primer_dict[off_target_primer_dict['{}:{}-{}'.format(chrom, off_target[0] + 1, off_target[1] + 1)][1]]
                        if similarity(Fprimer[0], read.query_sequence[:len(Fprimer[0])]) > 0.9:
                            if similarity(Rprimer[1], read.query_sequence[-len(Rprimer[0]):]) > 0.9:
                                off_target_locus_dict[chrom][(off_target[0], off_target[1])] += 1
                                identified = True
                        i = len(off_target_locus_dict[chrom])
                    else:
                        i += 1
            #If the read did not overlap with any known off-target loci, try to identify the primer sequences from the off-target read.
            if not identified:
                i = 0
                primers = [None, None]
                primer_list = list(primer_dict.keys())
                while i < len(primer_dict) and any(x == None for x in primers):
                    primer_ID = primer_list[i]
                    if similarity(primer_dict[primer_ID][0], read.query_sequence[:len(primer_dict[primer_ID][0])]) > 0.9:
                        primers[0] = primer_ID
                    elif similarity(primer_dict[primer_ID][1], read.query_sequence[-len(primer_dict[primer_ID][0]):]) > 0.9:
                        primers[1] = primer_ID
                    i += 1
                    
                #Add the locus to the off_target dict and primers list if both primers were identified.
                if not any(x == None for x in primers):
                    off_target_start, off_target_end = read_start + len(primer_dict[primers[0]][0]), read_end - len(primer_dict[primers[1]][1])
                    if off_target_start >= 0 and off_target_end >= 0 and off_target_end - off_target_start >= min_length:
                        if chrom not in off_target_locus_dict:
                            off_target_locus_dict[chrom] = dict()
                        if (off_target_start, off_target_end) not in off_target_locus_dict[chrom]:
                            off_target_locus_dict[chrom][(off_target_start, off_target_end)] = 1
                            off_target_primer_dict['{}:{}-{}'.format(chrom, off_target_start + 1, off_target_end + 1)] = [primers[0], primers[1]]
                        else:
                            off_target_locus_dict[chrom][(off_target_start, off_target_end)] += 1
    for chrom in off_target_locus_dict:
        i = 0
        loci = natsorted(list(off_target_locus_dict[chrom].keys()))
        while i < len(loci) - 1:
            if loci[i][1] > loci[i + 1][0]:
                if off_target_locus_dict[chrom][loci[i]] < off_target_locus_dict[chrom][loci[i + 1]]:
                    del off_target_locus_dict[chrom][loci[i]]
                    del off_target_primer_dict['{}:{}-{}'.format(chrom, loci[i][0] + 1, loci[i][1] + 1)]
                    del loci[i]
                else:
                    del off_target_locus_dict[chrom][loci[i + 1]]
                    del off_target_primer_dict['{}:{}-{}'.format(chrom, loci[i + 1][0] + 1, loci[i + 1][1] + 1)]
                    del loci[i + 1]
            else:
                i += 1
                    
    #Close the bam file.
    samfile.close()
    return off_target_locus_dict, off_target_primer_dict

def count_reads(bam, dir = args['bam_directory'], min_nr_reads = args['min_nr_reads']):
    '''
    Parse all bam files and count the number of reads that are aligned on target and (optionally) non-target loci in the reference genome sequence.
    '''
    #Extract the sample name.
    name = os.path.splitext(bam)[0]
    
    #Open the BAM file.
    samfile = pysam.AlignmentFile(dir + '/' + bam, 'rb')
    counts = list()
    
    #For each bam file, get all reads overlapping with amplicon regions.
    for chrom, loci in target_dict.items():
        for locus in loci:
            #Create a locus ID.
            ID = '{}:{}-{}'.format(chrom, locus[0] + 1, locus[1] + 1)
            
            #Count the number of reads in a region.
            Nr_reads = samfile.count(chrom, locus[0], locus[1])
            
            result = pd.DataFrame([[ID, Nr_reads]], columns = ['Locus', name]).set_index('Locus')
            counts.append(result)
    
    df = pd.concat(counts)
    
    #Close the bam file.
    samfile.close()
    return df

def df2smap(df, option = None, o = args['output_directory'], b = args['border_length'], s = args['suffix'], min_nr_reads = args['min_nr_reads']):
    new_gff = open('{}/SMAP_haplotype_window_OffTargets{}{}.gff'.format(o, s if s else '', '_RD{}'.format(min_nr_reads) if option == 'Filtered' else ''), 'w+')
    new_bed = open('{}/SMAP_haplotype_sites_OffTargets{}{}.bed'.format(o, s if s else '', '_RD{}'.format(min_nr_reads) if option == 'Filtered' else ''), 'w+')
    
    for locus, primers in df.iterrows():
        chrom = locus.split(':')[0]
        start, end = locus.split(':')[1].split('-')
        print('{}\tSMAP\tBorder_upstream\t{}\t{}\t.\t+\t.\tName={}'.format(chrom, int(start) - len(primer_dict[primers['Forward primer']][0]) - b, start, '{}:{}-{}_+'.format(chrom, start, end)), file = new_gff)
        print('{}\tSMAP\tBorder_downstream\t{}\t{}\t.\t+\t.\tName={}'.format(chrom, end, int(end) + len(primer_dict[primers['Reverse primer']][0]) + b, '{}:{}-{}_+'.format(chrom, start, end)), file = new_gff)
        print('{}\t{}\t{}\t{}\t.\t+\t{},{}\t.\t2\tHiplex_{}'.format(chrom, int(start) - 1, end, '{}:{}-{}_+'.format(chrom, start, end), start, end, s), file = new_bed)
    return

#===============================================================================
# Script
#===============================================================================

if __name__ == '__main__':

    print_date()
    
    #Create a dictionary with all information on the target loci and the primer sequences.
    target_dict = bed2dict()
    primer_dict = primers2dict()
    
    #Create a list of all BAM files in the BAM directory.
    bam_files = [f for f in os.listdir(args['bam_directory']) if f.endswith('bam')]
    
    #Extract primer sequences from primer file and identify off-target loci in all BAM files.
    if args['off_targets']:
        print(' * Identifying off-target loci in each BAM file ... ')
        with mp.Pool(args['processes']) as p:
            off_target_dicts = p.map(identify_off_targets, bam_files)
        
        #Reorganise the off_target loci of all samples into two dictionaries containing the locus coordinates (off_target_locus_dict) and the primer IDs (off_target_primer_dict).
        off_target_locus_dict, off_target_primer_dict = dict(), dict()
        for sample in off_target_dicts:
            for chrom, loci in sample[0].items():
                if chrom not in off_target_locus_dict:
                    off_target_locus_dict[chrom] = list()
                for locus in loci.keys():
                    if locus not in off_target_locus_dict[chrom]:
                        off_target_locus_dict[chrom].append(locus)
                        off_target_primer_dict['{}:{}-{}'.format(chrom, locus[0] + 1, locus[1] + 1)] = sample[1]['{}:{}-{}'.format(chrom, locus[0] + 1, locus[1] + 1)]
        
        #Reorder the off-target loci.
        off_target_locus_dict = {chrom : sorted(off_target_locus_dict[chrom]) for chrom in off_target_locus_dict}
        off_target_primer_dict = {locus : off_target_primer_dict[locus] for locus in sorted(off_target_primer_dict)}
        
        #Check if loci overlap. If loci overlap, only keep the largest locus.
        for chrom in off_target_locus_dict.keys():
            i = 0
            while i < len(off_target_locus_dict[chrom]) - 1:
                locus = off_target_locus_dict[chrom][i]
                next_locus = off_target_locus_dict[chrom][i + 1]
                if locus[1] > next_locus[0]:
                    if locus[1] - locus[0] < next_locus[1] - next_locus[0]:
                        del off_target_locus_dict[chrom][i]
                        del off_target_primer_dict['{}:{}-{}'.format(chrom, locus[0] + 1, locus[1] + 1)]
                    else:
                        del off_target_locus_dict[chrom][i + 1]
                        del off_target_primer_dict['{}:{}-{}'.format(chrom, next_locus[0] + 1, next_locus[1] + 1)]
                else:
                    i += 1
                    
    #Count the number of reads aligned to the target loci.
    print(' * Count all reads that are completely aligned to target loci ...')
    with mp.Pool(args['processes']) as p:
        target_dfs = p.map(count_reads, bam_files)
    
    #Concatenate the counts from all samples into one table and print the table to the output directory.
    target_df = pd.concat(target_dfs, axis = 1).fillna(0)
    target_df.to_csv('{}/Target_loci{}.tsv'.format(args['output_directory'], args['suffix'] if args['suffix'] else ''), sep = '\t')
    
    #Remove loci with less reads than the predefined minimum in every sample and print the filtered table to the output directory.
    filtered_indexes = target_df[target_df >= args['min_nr_reads']].dropna(how = 'all').index.dropna(how = 'all').tolist()
    target_df[target_df.index.isin(filtered_indexes)].to_csv('{}/Target_loci_DP{}{}.tsv'.format(args['output_directory'], args['min_nr_reads'], args['suffix'] if args['suffix'] else ''), sep = '\t')
    
    #Count the number of reads aligned to off-target loci.
    if args['off_targets']:
        print(' * Count all reads that are completely aligned to off-target loci ...')
        target_dict = off_target_locus_dict
        with mp.Pool(args['processes']) as p:
            off_target_dfs = p.map(count_reads, bam_files)
        
        #Concatenate the counts from all samples into one table and print the table to the output directory.
        off_target_df = pd.concat(off_target_dfs, axis = 1).fillna(0)
        off_target_df.to_csv('{}/Off-target_loci{}.tsv'.format(args['output_directory'], args['suffix'] if args['suffix'] else ''), sep = '\t')
        
        #Convert the primer dictionary into a data frame and print it to the output directory.
        primers_df = pd.DataFrame.from_dict(off_target_primer_dict, orient = 'index', columns = ['Forward primer', 'Reverse primer'])
        primers_df.index.name = 'Locus'
        primers_df.to_csv('{}/Off-target_primers{}.tsv'.format(args['output_directory'], args['suffix'] if args['suffix'] else ''), sep = '\t')
        
        #Remove loci with less reads than the predefined minimum in every sample from the off-target locus dictionary and print the table to the output directory.
        filtered_indexes = off_target_df[off_target_df >= args['min_nr_reads']].dropna(how = 'all').index.dropna(how = 'all').tolist()
        off_target_df[off_target_df.index.isin(filtered_indexes)].to_csv('{}/Off-target_loci_DP{}{}.tsv'.format(args['output_directory'], args['min_nr_reads'], args['suffix'] if args['suffix'] else ''), sep = '\t')
        
        #Remove loci with less reads than the predefined minimum in every sample from the off-target primers dictionary and print the table to the output directory.
        primers_df_filtered = primers_df[primers_df.index.isin(filtered_indexes)]
        primers_df_filtered.to_csv('{}/Off-target_primers_DP{}{}.tsv'.format(args['output_directory'], args['min_nr_reads'], args['suffix'] if args['suffix'] else ''), sep = '\t')
        
        #Create input tables with the coordinates from the off-target loci for SMAP haplotype.
        df2smap(primers_df)
        df2smap(primers_df_filtered, option = 'Filtered')
    
    print(' * Finished!\n')
    print_date()